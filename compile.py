#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import shutil
import subprocess


def main():
    for fn in sorted(os.listdir(os.getcwd())):
        if (
            fn.startswith((".", "_", "~"))
            or not fn.endswith(".md")
            or not fn.startswith("popr")
        ):
            continue
        print(f"processing {fn}")
        bn, _ = os.path.splitext(fn)
        new_tmp_fn = bn + ".html"
        new_fn = (
            f"/Users/pecheny/pecheny.me/static/popravki-2024-02-18/{bn[-1]}/index.html"
        )
        os.makedirs(os.path.dirname(new_fn), exist_ok=True)
        subprocess.check_call(["cpr", "-o", "html_water", fn])
        shutil.move(new_tmp_fn, new_fn)
        print(f"moved to {new_fn}")


if __name__ == "__main__":
    main()
