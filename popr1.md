[К другим поправкам](..)

Во фрагментах текста правил ~~зачёркиванием~~ обозначены удалённые фрагменты, ==выделением== — добавленные.

## 1. Переформулировка пункта 1.4.4 о розыгрыше вопроса, учитывающая аудиоверсии пакетов

*Текущий вариант пункта 1.4.4 не учитывает возможное использование аудиоверсий.*

> 1\. ведущий ~~объявляет~~ ==воспроизводит== номер вопроса;
>
> 2\. в случае наличия дополнительных материалов техническая группа обеспечивает командам доступ к ним, в том числе==, если необходимо,== запускает их воспроизведение. Рекомендуется дать командам время на ознакомление с материалами. ~~Регламент соревнования может допускать отправку дополнительных материалов на электронные устройства, находящиеся на игровых местах команд;~~
>
> 3\. ведущий ~~зачитывает~~ ==воспроизводит== текст вопроса и ~~даёт команду «Время!»~~ ==запускает отсчёт времени, сообщив командам о начале отсчёта с помощью команды «Время» или звукового сигнала==;
>
> 4\. если вопрос состоит из нескольких подвопросов, текст очередного подвопроса ~~зачитывается~~ ==воспроизводится== после истечения времени на обсуждение предыдущего подвопроса;
>
> 5\. техническая группа может ~~начать сбор ответов у команд, если команда досрочно сигнализирует о готовности ответа~~ ==досрочно принять ответ у команды, если команда сигнализирует о его готовности==. Запрещается изменение ответа, сданного технической группе;
>
> 6\. за 10 секунд до окончания времени на поиск ответа ведущий ~~предупреждает команды~~ ==воспроизводит предупреждение== об этом;
>
> 7\. по истечении времени на поиск ответа ведущий ~~даёт команду «Время!»~~ ==воспроизводит сигнал об окончании минуты== и начинает обратный десятисекундный отсчёт от «10» до «0»;
>
> 8\. в это время команды записывают и фиксируют ответы. Регламент соревнования может допускать сдачу ответов при помощи электронных устройств. После объявления счёта «0» изменение или написание ответа запрещено;
>
> 9\. после объявления счёта «0» техническая группа начинает принимать ответы у команд;
>
> 10\. после окончания приёма ответов у команд ведущий ~~объявляет~~ ==воспроизводит== авторский ответ и другую сопутствующую информацию. Рекомендуется ~~озвучивать~~ ==воспроизводить== критерии зачёта и комментарий.
>
> ==Для воспроизведения любой текстовой информации ведущий может использовать как собственный голос, так и аудио- или видеозаписи, а также, если это предусмотрено регламентом турнира, экраны электронных устройств, в том числе находящихся на игровых местах команд. Если это предусмотрено регламентом турнира, в тексты, выводимые на экраны электронных устройств, могут быть внесены изменения по сравнению с текстом пакета для ведущего.==