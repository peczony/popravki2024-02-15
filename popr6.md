[К другим поправкам](..)

Во фрагментах текста правил ~~зачёркиванием~~ обозначены удалённые фрагменты, ==выделением== — добавленные.

## 6. Мультиязычные поправки

*Поправки внесены молодёжной группой (автор — Евгений Миротин). Правки регламентируют проведение мультиязычных турниров (которые теперь могут считаться одним турниром, а не несколькими разными). Цель — увеличить количество мультиязычных турниров, что отвечает целям организации (п. 2.1.5 [Устава](https://www.maii.li/statute/ru)).*

### Изменения п. 2 «Виды игр»

*Выделяется подпункт 2.1 «Формы проведения». Пункты, ранее носившие номера 2.1–2.5, теперь носят номера 2.1.1–2.1.5. Вводится новый подпункт 2.2 «Мультиязычный турнир»:*

> Мультиязычный турнир — разновидность турнира, участники которого на различных площадках могут играть версии пакета вопросов на разных языках (далее — языковые версии). Мультиязычным может быть лишь турнир, проводимый более чем на одной площадке — строго синхронный турнир, заочный турнир, строго синхронный онлайн-турнир либо заочный онлайн-турнир. При этом каждый отыгрыш на отдельно взятой площадке проводится на одной языковой версии пакета.
>
> При подготовке пакета вопросов мультиязычного турнира должны выполняться следующие требования:

> -   доступная командам перед отыгрышем информация должна явно указывать, на каком языке или языках пакет был подготовлен изначально, и на каких он доступен для отыгрыша;
> -   редакционная группа готовит основную языковую версию пакета (допускаются ситуации, когда отдельные части пакета могут быть подготовлены на различных языках);
> -   переводчики в сотрудничестве с редакторами основной версии готовят переводные языковые версии пакета;
> -   при оформлении каждой языковой версии (всего пакета или его отдельной части) должно быть явно указано, является ли она основной (оригинальной) или переводной, язык оригинала, а в случае перевода также язык перевода и список переводчиков.

> При переводе вопросов переводчики должны руководствоваться принципами лингвистической и функциональной эквивалентности.
>
> Под лингвистической эквивалентностью подразумевается, что переводная версия должна быть корректным переводом в том же смысле, в каком это относится к переводам других текстов, в том числе литературных и технических.
>
> Под функциональной эквивалентностью подразумевается, что оригинал и перевод содержат одни и те же приёмы, заложенные редакторами, такие как смысловые замены, смысловые подсказки, технические отсечки указанием на число слов в ответе и т.п. Переводная версия не должна ни упускать, ни добавлять новые подсказки или способы нахождения ответа. В частности, этот пункт означает, что редакторам и переводчикам следует максимально учитывать особенности всех языков, на которые переводится пакет, в части таких приёмов, как:

> -   слова, указывающие на грамматический род объектов (в том числе местоимения ОН/ОНА/ОНО, изменяющиеся по родам прилагательные и т.д.) — так как ряд языков не имеет этой категории, что осложняет поиск ответа в сравнении с языками, имеющими грамматический род;
> -   указания на число слов в ответе — кроме случая, когда реалия имеет однозначный перевод на каждый язык, и для каждого языка может быть надёжно определено число слов в ответе (оно может быть разным для разных языков);
> -   указания на фонетические и графемные особенности ответа (слова, начинающиеся на одну букву, парные согласные и т.д.) — за исключением редких ситуаций, когда это свойство гарантированно выполняется для всех языковых версий — например, в явном виде указано, что ответ следует дать на латыни или на английском языке;
> -   и т.п.

> В общем случае редакторам рекомендуется отказаться от подобных подсказок в пакетах, предназначенных для перевода, и использовать их с максимальной осторожностью и только в случае необходимости.

### Изменения пункта 1.1 «Определение и принципы игры»

*В конец пункта добавляется информация об эквивалентности различных языковых версий вопроса.*

> Игра может состоять из одного этапа или быть разбита на несколько этапов. Каждый этап игры не может состоять менее, чем из 6 вопросов. Команда, правильно ответившая на большее количество вопросов отдельного этапа игры, всегда занимает на данном этапе более высокое место, чем команда, правильно ответившая на меньшее количество вопросов. Если команды участвовали в одних и тех же этапах, команда, сыгравшая лучше каждый этап, всегда побеждает команду, сыгравшую хуже каждый этап.
>
> ==При проведении мультиязычных турниров в контексте данных правил все языковые версии вопроса считаются одним вопросом с общими критериями оценки правильности ответов.==

### Изменения пункта 1.2.2 «Редакционная группа»

*Добавляется положение о том, что переводчики пакета входят в редакционную группу.*

> Редакционная группа включает в себя редакторов пакета и авторов вопросов. Редакторы готовят пакет вопросов для проведения соревнования, основываясь на фактах и идеях авторов вопросов. Редакторы предоставляют организационной группе список авторов.
>
> ==При проведении мультиязычных турниров в редакционную группу включаются также переводчики отдельных языковых версий пакета.==
>
> Запрещается совмещать роли члена редакционной группы и апелляционного жюри.

### Изменения пункта 1.6 «Апелляции»

*В список допустимых апелляций добавляется апелляция на ошибку в переводе вопроса:*

> Допускаются апелляции следующих видов:

> -   на зачёт ответа команды;
> -   на некорректность вопроса;
> -   на ошибку в воспроизведении вопроса;
> -   ==на ошибку в переводе вопроса==.

*Подпункт 1.6.4 «Порядок рассмотрения апелляций» изменяет номер на 1.6.5. Перед ним добавляется подпункт 1.6.4 «Апелляция на ошибку в переводе вопроса»:*

> Апелляции на ошибку в переводе вопроса принимаются только на мультиязычных турнирах и только если это предусмотрено регламентом турнира.
>
> Апелляционное жюри удовлетворяет апелляцию на ошибку в переводе вопроса в случае, если:

> -   одна из языковых версий вопроса в процессе перевода утратила корректность;
> -   одна из языковых версий вопроса в процессе перевода стала существенно более простой или существенно более сложной, чем основная версия вопроса.

> В регламенте может быть указана минимальная доля от общего количества сыгравших турнир команд, которых коснулась подобная ошибка, для того, чтобы команда имела право подать такую апелляцию. Если такая доля в регламенте не указана, она принимается равной 2%.
>
> Если апелляционное жюри удовлетворяет апелляцию на ошибку в переводе вопроса, данный вопрос не учитывается при подсчёте правильных ответов для всех команд, участвующих в соревновании.